# Readme #

The following collection is a living collection of Zotero styles.

If no other license is given in the credits below, the main license is valid.

## Credits: ##

### FB03: ###
Thanks to Stanislaw Koltschin, who created the style <a href="http://www.zotero.org/styles/hochschule-fur-wirtschaft-und-recht-berlin">"Hochschule für Wirtschaft und Recht Berlin (German)"</a>
       